import error from 'src/Error'
import {Routes} from 'src/Route'
module.exports = {
    error: error,
    Routes: Routes
}