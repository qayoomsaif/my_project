import { UserInfoStorage } from 'Factory'
import Factory from 'src/Factory'
export default class DataServices {
    constructor(service) {
        this.service = service
        // console.log(Factory)
    }

    async getDataFromApi(url) {
        if(!url){
            return 
        }
        var response = await this.service.GET(url)
        return response
    }
}