import { Service } from 'src/Factory'
import DataServices from './DataServices'
import error from 'src/Error'
var Services = {
    dataServices: new DataServices(Service),
}

module.exports = { Services: Services, error: error }
