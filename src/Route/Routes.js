import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import HomeComponent from 'src/Screen/Home/home.component';
import HiddenComponent from 'src/Screen/Hidden/hidden.component';
const Routes = () => {
    return (
        <Router>
            <Scene key="root">
                <Scene key="homeComponent" component={HomeComponent} hideNavBar  panHandlers={null} initial />
                <Scene key="hiddenComponent" component={HiddenComponent} />
            </Scene>
        </Router>
    );
}

module.exports = { Routes };