import React, { Component } from 'react';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import map_icon_1 from 'src/assets/newImage/location_icon.png';
import { styles, MapController, MapDefaultStyle } from './index'
import { View, Text } from 'react-native'

export default class MapComponent extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <MapView
                style={this.props.style || styles.map}
                initialRegion={{
                    latitude: this.props.lat? parseFloat(this.props.lat) : 0,
                    longitude: this.props.lng?parseFloat(this.props.lng) : 0,
                    latitudeDelta: this.props.latitudeDelta?parseFloat(this.props.latitudeDelta) : 0.00922,
                    longitudeDelta: this.props.longitudeDelta?parseFloat(this.props.longitudeDelta) : 0.004
                }}
            minZoomLevel={5}
            >
                <MapView.Marker
                    coordinate={{
                        latitude: this.props.lat? parseFloat(this.props.lat) : 0,
                        longitude: this.props.lng?parseFloat(this.props.lng) : 0,
                        latitudeDelta: this.props.latitudeDelta?parseFloat(this.props.latitudeDelta) : 0.00922,
                        longitudeDelta: this.props.longitudeDelta?parseFloat(this.props.longitudeDelta) : 0.00421
                    }}
                    image={this.props.mapIcon || map_icon_1}
                    title={this.props.title || null}
                />
 <Text style ={styles.headline}>
 {this.props.title || null}
</Text>
            </MapView>

        )
    }





}
module.exports = { MapComponent }