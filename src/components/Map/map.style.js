import { Dimensions,StyleSheet } from 'react-native';
const { width, height } = Dimensions.get('window');
import { resize , color} from 'src/Utilities/styleConfig';
const styles = StyleSheet.create({
    container: {
    ...StyleSheet.absoluteFillObject,
    // height: 400,
    // width: 400,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
    maincontainer: {
        height: height,
        flex: 1,

    },
    map: {
        flex: 1,
        height: height,
        width: width,
    },
   
    headline: {
        fontSize: resize.moderateScale(30),
        margin: 10,
        color: color.appColor,
    },
});



module.exports = { styles }