import {
    resize,
    color,
} from 'src/Utilities/styleConfig';
const styles = {
    maincontainer: {
        backgroundColor: color.white,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor:color.blue,
        marginBottom: resize.verticalScale(10),
        margin: resize.squire(5)
    },

    imagecontainer: {
        height: resize.squire(200),
        width: resize.squire(170),
        backgroundColor: 'grey'
    },
    detailcontainer:{
        height: resize.squire(200),
        padding: resize.squire(10),
        width: resize.width-resize.squire(170),
    },
    image: {
        height: resize.squire(200),
        width: resize.squire(170),
        justifyContent: 'flex-end'
    },
    imageinnercontainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    innercontainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: resize.squire(5),
    },
    imagecountcontianer:{
        marginBottom:resize.squire(5),
        marginLeft:resize.squire(5),
        flexDirection: 'row',
        width: resize.squire(40),
        height: resize.squire(25),
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        borderRadius:resize.squire(5)
    },
    imagemapcontianer:{
        marginBottom:resize.squire(5),
        marginRight:resize.squire(5),
        flexDirection: 'row',
        width: resize.squire(30),
        height: resize.squire(25),
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)' ,
        borderRadius:resize.squire(5)

    },
    iconimage:{
        // margin: resize.squire(10),
        height: resize.squire(15),
        width: resize.squire(15),
    },
    deletecontainer:{
        alignSelf:'flex-end'
    },
    pkrtext:{
        fontSize: resize.squire(14)  
    },
    titletext:{
        marginTop:resize.squire(8),
        fontSize: resize.squire(14),
        fontWeight:"600",
        paddingLeft: resize.squire(5),

    },
    deleteiconimage:{
        height: resize.squire(20),
        marginLeft: 10,
        width: resize.squire(20),
        marginRight:resize.squire(5)
    },
    badroomcontainer:{
        
        marginTop:resize.squire(5),
        flexDirection: 'row',
        alignItems:'center',
    },
    generictext:{
        marginLeft: resize.squire(5),
        fontSize: resize.squire(14)  
    },
    postdatetext:{
        // marginLeft: resize.squire(5),
        // alignSelf:'flex-end',
        position:'absolute',
        bottom: resize.squire(5),
        left:resize.squire(15),
        fontSize: resize.squire(14)  
    },
    agenticonimage:{
        // marginLeft: resize.squire(5),
        // alignSelf:'flex-end',
        position:'absolute',
        bottom: resize.squire(5),
        right:resize.squire(15),
        width: resize.squire(50),
        height:resize.squire(50)  
    },
    marlatext:{
        marginTop:resize.squire(8),
        marginLeft: resize.squire(5),
        fontSize: resize.squire(14),
        color:color.blue 
    },
    descriptiontext:{
        marginTop:resize.squire(15),
        marginLeft: resize.squire(5),
        fontSize: resize.squire(14),
        // color:'#0080FF' 
    },
    imagecount:{marginLeft: resize.squire(5),
        fontSize: resize.squire(14),
        color: '#ffffff'
    },    
    genericiconimage:{
        height: resize.squire(20),
        width: resize.squire(20),
    },
    badroomsubcontianer:{

    },
    main2: {
        // marginRight: resize.moderateScale(16),
        // marginLeft: resize.moderateScale(16),
        // borderRadius: resize.moderateScale(6),
        // marginRight: resize.moderateScale(16),
        // marginLeft: resize.moderateScale(16),
        // marginBottom: resize.moderateScale(5),
        // alignSelf:'center',
        // borderWidth: 0.5,
        // justifyContent: 'center',
        // borderColor: '#e5e5e5',
        // backgroundColor: color.white,
    },
    card: {
        // borderRadius: resize.moderateScale(6),
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },


    line2: {
        // marginTop: resize.squire(10),
        // marginBottom: resize.squire(15),
        borderWidth: 0.5,
        borderColor: '#e7e7e7',
        width: resize.width
    },
    line: {

        // marginTop: resize.squire(5),
        // marginBottom: resize.verticalScale(10),
        borderWidth: 0.5,
        borderColor: '#e7e7e7',
        width: resize.width,
        // marginBottom: resize.moderateScale(5),

        // position:'absolute',
        // left:0,
        // right:0,

        // bottom: resize.squire(-15)
    },
    centercontainer: {
        // marginLeft: resize.moderateScale(13),
        // height: resize.squire(128),
        // alignItems: 'center',
        // flexDirection: 'row',
    },
    titleandinfocolumn: {
        // width: resize.moderateScale(230),
        // height: resize.squire(60),
        // marginTop: resize.verticalScale(5),
    },
    titleandinfocolumn2: {
        // width: resize.moderateScale(190),
        // marginTop: resize.verticalScale(18),
    },
    dateandheartcolumn: {
        // width: resize.moderateScale(54),
        // // height: resize.moderateScale(54)
    },

    rightcontainer: {
        // marginLeft: resize.moderateScale(13),
        // width: resize.moderateScale(54),
        alignItems: 'center',
        flexDirection: 'row',
    },
    datemaincontainer: {
        // marginTop: resize.verticalScale(18),
        // marginLeft: resize.moderateScale(17),
        // height: resize.squire(54),
        // width: resize.squire(54),
        // backgroundColor: '#F0F0F0',
        // justifyContent: 'center',
        // alignItems: 'center',
        // borderRadius: resize.verticalScale(14)
    },
    favmaincontainer: {
        // marginLeft: resize.moderateScale(17),
        // height: resize.squire(54),
        // width: resize.squire(54),
        alignItems: 'center',
        justifyContent: 'center',
    },
    fav: {
        // marginTop: resize.verticalScale(16),
        // height: resize.squire(40),
        // width: resize.squire(40),
        alignItems: 'center',
        // justifyContent: 'center',
    },
    favimage: {
        height: resize.squire(24),
        width: resize.squire(24)
    },



}

export default styles;