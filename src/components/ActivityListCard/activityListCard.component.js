import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, Image, ImageBackground } from 'react-native';
import styles from "./activityListCard.style";
import moment from 'moment';
import {Helper} from 'Factory';
import { ModalListMain } from 'components'

export default class ActivityListCard extends PureComponent {
    constructor(props) {
        super(props);
        this.state= {isModalVisible: false}
    }
    changeDateFormat(date) {
        try {
            let oldFormat = "YYYY-MM-DD HH:mm:ss"
            let newFormat = 'MMM DD, Y'
            let response = moment(date, oldFormat).format(newFormat)
            if (response != 'Invalid date') {
                return response
            }
            return ''
        } catch (e) {
            return ''
        }
    }
    getImage(str) {
        var array = str.split(",");
        if (array[0]) {
            return 'https://wordskill.website/ghar-backend/public/uploads/property-images/thumbnails/' + array[0]
        }
        return null
    }
    getImageCount(str) {
        var array = str.split(",");
        if (array[0]) {
            return array.length
        }
        return null
    }

    render() {
        const { data } = this.props
        return (
            <View style={styles.maincontainer}>
                <View style={styles.imagecontainer}>
                    <ImageBackground
                        style={styles.image}
                        source={{ uri: this.getImage(data.all_images) }}
                    >
                        <View style={styles.imageinnercontainer}>
                            <TouchableOpacity style={styles.imagecountcontianer} >

                                <Image
                                    style={styles.iconimage}
                            resizeMode={'contain'}
                                    source={require('src/assets/newImage/green_camera_icon.png')}
                                />
                                <Text style={styles.imagecount}>{this.getImageCount(data.all_images)}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.imagemapcontianer}
                                onPress={() =>
                                    data.address?this.setState({isModalVisible:true}): Helper.alert('address not found')}
                            >
                                <Image
                                    style={styles.iconimage}
                            resizeMode={'contain'}
                            source={require('src/assets/newImage/location_icon.png')}
                                />
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                </View>
                <View
                    style={styles.detailcontainer}
                >
                    <View style={styles.innercontainer}>
                        <Text style={styles.pkrtext}>
                            {'PKR ' + data.price}
                        </Text>
                        <TouchableOpacity
                            style={styles.deletecontainer}
                            onPress={() => this.props.onPressDelete ? this.props.onPressDelete(this.props.data) : null}>
                            <Image
                                style={styles.deleteiconimage}
                            resizeMode={'contain'}
                            source={require('src/assets/newImage/delete_img_icon.png')} />
                        </TouchableOpacity>

                    </View>
                    <Text style={styles.titletext}
                    >
                        {data.title}
                    </Text>
                    <View style={styles.badroomcontainer}>
                        {data.bedrooms ?
                            <Image
                                style={styles.deleteiconimage}
                            resizeMode={'contain'}
                            source={require('src/assets/newImage/home.png')} />
                            : null}
                        {data.bedrooms ?
                            <Text style={styles.generictext}>
                                {data.bedrooms}
                            </Text>
                            : null}
                        {data.bathrooms ?
                            <Image
                                style={styles.deleteiconimage}
                            resizeMode={'contain'}
                            source={require('src/assets/newImage/download_icon.png')} />
                            : null}
                        {data.bathrooms ?
                            <Text style={styles.generictext}>
                                {data.bathrooms}
                            </Text>
                            : null}
                        {data.size_in_marla ?
                            <Image
                                style={styles.deleteiconimage}
                            resizeMode={'contain'}
                            source={require('src/assets/newImage/calender.png')} />
                            : null}
                        {data.size_in_marla ?
                            <Text style={styles.generictext}>
                                {parseInt(data.size_in_marla).toFixed(0) + ' marla'}
                            </Text>
                            : null}
                    </View>
                    {data.size_in_marla ?
                        <Text style={styles.marlatext}>
                            {parseInt(data.size_in_marla).toFixed(0) + ' marla for sale'}
                        </Text>
                        : null}
                    {data.description ?

                        <Text style={styles.descriptiontext} numberOfLines={2}>
                            {data.description}
                        </Text>
                        : null}

                    {data.created_at ?
                        <Text style={styles.postdatetext} numberOfLines={2}>
                            {'Posted on ' + this.changeDateFormat(data.created_at)}
                        </Text>
                        : null}
                    {data.is_agent === 1 ?
                        <Image
                            style={styles.agenticonimage}
                            resizeMode={'contain'}
                            source={require('src/assets/newImage/cross.png')} />
                        : null}

                </View>
                {this.state.isModalVisible?
                    <ModalListMain isModalVisible ={this.state.isModalVisible} address = {data.address} _cancel = {()=>this.setState({isModalVisible:false})}/>
                :null}
            </View>
        );
    }
};

module.exports = { ActivityListCard }