import React, { Component } from 'react';
import {
    FlatList, Text, View, Image, TouchableOpacity,
} from 'react-native';
import Styles from "./modallistmain.stlye";
import Modals from 'react-native-modal';
import {MapComponent} from '../Map/map.component'
export default class ModalListMain extends Component {
    constructor(props) {
        super(props);
        var { isModalVisible } = props
        this.state = {
            isModalVisible:this.props.isModalVisible
        }
    }

  
    render() {
        return (
            < Modals
            
                onBackdropPress={() => { this.props.isModalVisibleHide ?  this.props.isModalVisibleHide : null }}
                style={styles.modalmaincontainer}
                isVisible={this.state.isModalVisible} >
        <View style={styles.modaltopcontainer}>
                    <TouchableOpacity style={styles.backbuttoncontainer}
                        onPress={() => this.props._cancel ? this.props._cancel() : null}
                    >
                        <View style={styles.buttonStyle}>
                            <Image source={require('src/assets/newImage/left_arrow.png')} 
                            resizeMode="contain" 
                            style={styles.image} />
                        </View>
                    </TouchableOpacity>
                    </View>

            <MapComponent lat= {33.6844}lng = {73.0479} title = {this.props.address}/>
            </Modals >
        )
    }
}
const styles = Styles;
export { ModalListMain };