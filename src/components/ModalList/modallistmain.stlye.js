
import { font, resize, fontColor, color } from 'src/Utilities/styleConfig';
//
const styles = {
   
    modalmaincontainer: {
        backgroundColor: 'white',
        alignItems: 'flex-start',
        flex: 1
    },
    modalmaincontainer: {
        backgroundColor: 'white',
        alignItems: 'flex-start',
        flex: 1,
        margin: resize.moderateScale(-3, 0.5)
    },
    modaltopcontainer: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: resize.verticalScale(20),
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: resize.moderateScale(4, 0.5),
    },
    backbuttoncontainer: {
        height: resize.moderateScale(50),
        width: resize.moderateScale(70),
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingLeft: resize.moderateScale(12, 1),
    },
    buttonStyle: {
        height: resize.moderateScale(20, 1),
        width: resize.moderateScale(20, 1),
        opacity: 0.75,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        marginLeft: resize.moderateScale(20),
        height: resize.squire(20),
        width: resize.squire(20),
    },
}
export default styles;