import {  NAME_CHANGED, EMAIL_CHANGED, PASSWORD_CHANGED, CONFIRM_PASSWORD_CHANGED,HIDDEN_DATA_CHANGE  } from '../actions/type';
const INITIAL_STATE = { name: 'mae', email: 'qayoom', password: '', confirmPassword: '' , hiddenData:[]};

export default (state=INITIAL_STATE, action) => {
    switch (action.type){
        case NAME_CHANGED:
        return {  ...state, name: action.payload  }
        case HIDDEN_DATA_CHANGE:
            return {  ...state, hiddenData: action.payload  }
        case EMAIL_CHANGED:
        return {...state, email: action.payload}
        case PASSWORD_CHANGED:
        return {...state, password: action.payload}
        case CONFIRM_PASSWORD_CHANGED:
        return {...state, confirmPassword: action.payload}
    default:
     return state;
}
}