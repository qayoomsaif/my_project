import ApplicationFactory from './ApplicationFactory'
const factory = ApplicationFactory()

// Whenever we need a Manager function, we will import factory and write the name of the desired key. In this way we will be able to call any manager function via factory!
module.exports = {
    Router: factory.getRouter(),
    Helper: factory.getHelper(),
    StorageManager: factory.getStorageManager(),
    ServiceMain: factory.getServiceMain(),
    Service: factory.getService(),
}

