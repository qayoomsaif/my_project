import {
    StorageManager, Helper,ServiceMain,Service
} from 'src/Manager'
import { Router } from 'src/Route'
var applicationFactory = null

class ApplicationFactory {
    constructor() {
        // this.config = config
        // Assigning all functions of Manager class to the instance of this class
        this.router = new Router()
        this.helper = new Helper()
        this.storageManager = new StorageManager()
        this.serviceMain = new ServiceMain()
        this.service = new Service(this.serviceMain)
    }

    /* When these below functions will be called from anywhere in the project they will call Manager functions  */
    getRouter() {
        return this.router
    }
    
    getStorageManager() {
        return this.storageManager
    }
    getService() {
        return this.service
    }
    getServiceMain() {
        return this.serviceMain
    }
    getHelper() {
        return this.helper
    }
}

/**
 * @param {*} config 
 * @returns {ApplicationFactory}
 */
function getFactory() {
    if (!applicationFactory) {
        // if (!config) {
            // throw new Error("config not defined")
        // }
        applicationFactory = new ApplicationFactory()
    }
    return applicationFactory
}
module.exports = getFactory
