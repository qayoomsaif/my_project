import React, { Component } from 'react';
import { View, Text, TouchableOpacity, BackHandler, NetInfo, Image } from 'react-native';
import ExtendableBuiltin from 'src/Error/ExtendableBuiltin'
import { UserInfoStorage, Router, Analytics, DeviceInfo } from 'src/Factory'

class SuperMainComponent extends Component {
    constructor(props) {
        super(props)
        // console.log(Component)
        state = {
            isConnected: false,
            isLogin: UserInfoStorage.isLogin
        };
        this.getIsLogin = this.getIsLogin
        // this.getIsLogin()
    }
    componentDidMount() {
        // console.log('SuperMainComponent')
        // NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }

    componentWillUnmount() {
        // NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }
    componentDidMount() {
        Analytics.youcan.screen('SuperMainComponent', SuperMainComponent.name);
        if (DeviceInfo.platform.android) { BackHandler.addEventListener('SuperMainComponent', this.handleBackButton.bind(this)) }
    }

    componentWillUnmount() {
        if (DeviceInfo.platform.android) { BackHandler.removeEventListener('SuperMainComponent', this.handleBackButton.bind(this)) };
    }

    handleBackButton() {
        Router.loginComponent()
    }
    // analytic (name){

    // }
    // BackHandler(name){

    // }
    getIsLogin() {
        // console.log('isLogin: UserInfoStorage.isLogin')
        this.setState({ isLogin: UserInfoStorage.isLogin })
    }
    static onEnter() {

    }
    static onExist() {

    }

}
module.exports = SuperMainComponent 