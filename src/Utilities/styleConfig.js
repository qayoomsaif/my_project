import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

const scale = size => width / 410 * size;
const scaleHeight = size => height / 820 * size
var styleConfig = {
    color: {
        font: {
            blue: '#1E90FF',
            black: '#000000',
            white: '#FFFFFF',
            greyLight: '#999999',
            greyDark: '#808080',
            grey: '#B3B3B3',
            grey2: '#6B6B6B',
            appColor: '#27AE60',
            lightRed: '#f73434'
        },
        background: {
            white: '#FFFFFF',
            greyLight: '#F7F7F7',
            greyDark: '#808080',
            blue: '#1E90FF',
            appColor: '#27AE60',
        }
    },


    resize: {
        width: width,
        height: height,
        scale: size => width / 410 * size,
        scaleHeight: size => height / 820 * size,
        verticalScale: (size, factor = 1) => size + (scaleHeight(size) - size) * factor,
        moderateScale: (size, factor = 1) => size + (scale(size) - size) * factor,
        squire: (size) => height < width ? size + (scaleHeight(size) - size) : size + (scale(size) - size),
        moderateScaleHalf: (size, factor = 1) => (size + (scale(size) - size) * factor) / 2,
        moderateScaleThird: (size, factor = 1) => (size + (scale(size) - size) * factor) / 3,
        moderateScaleForth: (size, factor = 1) => (size + (scale(size) - size) * factor) / 4,
        widthHalf: (size) => width / 2,
        widthThird: (size) => width / 3,
        widthForth: (size) => width / 4,
    }

}

module.exports = {
    styleConfig: styleConfig,
    resize: styleConfig.resize,
    fontColor: styleConfig.color.font,
    color: styleConfig.color.background,
};
