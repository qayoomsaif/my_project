import React, { Component } from 'react';
import { Routes } from './index';
import { Provider } from 'react-redux';
import reducers from 'src/reducers';
import {  createStore  } from 'redux';

class App extends Component {
    render() {
        return (
            // <View style={{flex: 1}}>

    <Provider store={createStore(reducers)}>

            <Routes />
                  </Provider>
                //   </View>

        );
    }
}

export default App;