import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, Dimensions, RefreshControl } from 'react-native';
import { ActivityListCard } from 'components'
import styles from './hidden.style'
import { connect } from 'react-redux';
import { hiddenDataChange } from 'src/actions';
class HiddenComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false
    };
  }
  async deleteIndex(item) {
  console.log('does not delete')
  }
  _renderRow(rowData) {
    return (
      <ActivityListCard data={rowData} onPressDelete={(item) => this.deleteIndex(item)} />
    );
  }
  renderFooter() {
    return (
      //Footer View with Load More button
      <View style={styles.footer}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={this.loadMoreData}
          //On Click of button calling loadMoreData function to load more data
          style={styles.loadMoreBtn}>
          <Text style={styles.btnText}>Loading</Text>
          {this.state.fetching_from_server ? (
            <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
          ) : null}
        </TouchableOpacity>
      </View>
    );
  }
  render() {
    return (
      <View style={{ flex: 1}}>
        {this.props.hiddenData.length?  
        <FlatList
            style={{ flex: 1}}
            data={this.props.hiddenData}
            renderItem={item => this._renderRow(item.item)}
            scrollEventThrottle={16}
            ItemSeparatorComponent={() => <View style={styles.separator} />}
          /> :
          <Text style={{fontSize:30}}>
            {'no record'}
            </Text>
            }
      </View>
    );
  }
}

const mapStateToProps = ({ account }) => {
  const { hiddenData } = account;
  return { hiddenData };
};

export default connect(mapStateToProps, { hiddenDataChange })(HiddenComponent);