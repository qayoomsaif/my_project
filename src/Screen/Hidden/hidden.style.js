
import {color,fontColor, resize } from 'src/Utilities/styleConfig';
import {StyleSheet} from 'react-native';

// 27AE60
const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      height: 400,
      width: 400,
      justifyContent: 'center',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
    maincontainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#27AE60',
    },
    logoicon: {
        marginTop: resize.verticalScale(200),
        width: resize.squire(100),
        height: resize.squire(100),
    },
    animatedBox: {
        flex: 1,
        backgroundColor: "#38C8EC",
        padding: 10
      },
      body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F04812'
      },
    subtext:{
        marginTop: resize.verticalScale(45),
        // fontFamily: font.fontTwo.semiBold,
        fontSize: resize.moderateScale(18),
        textAlignVertical: 'center',
        textAlign: 'center',
        color: '#FFF',  
    },
    item: {
        padding: 10,height:80
      },
      separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
      },
      text: {
        fontSize: 15,
        color: 'black',
      },
      footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
      },
      loadMoreBtn: {
        padding: 10,
        backgroundColor: '#800000',
        borderRadius: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      },
      btnText: {
        color: 'white',
        fontSize: 15,
        textAlign: 'center',
      },
        image:{
        width: resize.width,
        height: resize.verticalScale(160),
    },
    
});

export default styles;