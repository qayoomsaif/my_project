import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, Dimensions, RefreshControl, ActivityIndicator, Image } from 'react-native';
import styles from "./home.style";
import HomeController from "./home.controller";
import { ActivityListCard } from 'components'
const { height } = Dimensions.get('window');
import { Router } from 'Factory'
const SCREEN_HEIGHT = height
import ParallaxScrollView from 'react-native-parallax-scrollview';
import { connect } from 'react-redux';
import MenuDrawer from 'react-native-side-drawer'
import { hiddenDataChange } from 'src/actions';


import FCM, { NotificationActionType } from "react-native-fcm";

import { registerKilledListener, registerAppListener } from "./Listeners";
registerKilledListener();

class HomeComponent extends Component {
  constructor(props) {
    super(props);
    this.controller = new HomeController(this)
    this.state = {
      listData: [],
      open: false,
      refreshing: false,
      loadBottom: false,
      nextUrl: null,
      token: "",
      tokenCopyFeedback: ""
    };
  }

  async componentDidMount() {
    //FCM.createNotificationChannel is mandatory for Android targeting >=8. Otherwise you won't see any notification
    FCM.createNotificationChannel({
      id: 'default',
      name: 'Default',
      description: 'used for example',
      priority: 'high'
    })
    registerAppListener(this.props.navigation);
    FCM.getInitialNotification().then(notif => {
      this.setState({
        initNotif: notif
      });
      if (notif && notif.targetScreen === "detail") {
        setTimeout(() => {
          this.props.navigation.navigate("Detail");
        }, 500);
      }
    });

    try {
      let result = await FCM.requestPermissions({
        badge: false,
        sound: true,
        alert: true
      });
    } catch (e) {
      console.error(e);
    }

    FCM.getFCMToken().then(token => {
      console.log("TOKEN (getFCMToken)", token);
      this.setState({ token: token || "" });
    });

    if (Platform.OS === "ios") {
      FCM.getAPNSToken().then(token => {
        console.log("APNS TOKEN (getFCMToken)", token);
      });
    }
  }



  async onRefresh() {
    this.setState({
      listData: [], refreshing: false
    }, async () => {
      this.controller.getData('https://wordskill.website/ghar-backend/public/api/get-lists')
      await this.props.hiddenDataChange([])
    }
    )
  }

  async onHiddenDataChange(data) {
    await this.props.hiddenDataChange([...this.props.hiddenData, ...data]);
  }
  componentWillMount() {
    console.ignoredYellowBox = ['Remote debugger'];
    console.disableYellowBox = true;
    this.controller.getData('https://wordskill.website/ghar-backend/public/api/get-lists')
  }
  setData(data, nextUrl) {
    this.setState({ listData: [...this.state.listData, ...data], nextUrl: nextUrl, loadBottom: false })

  }
  async deleteIndex(item) {
    var arr = this.state.listData
    arr = arr.filter(function (el) {
      return el.id !== item.id;
    });
    if (this.state.listData.length !== arr.length) {
      await this.onHiddenDataChange([item])
      this.setState({ listData: arr })
    }

  }
  loadMoreData() {
    if (this.state.nextUrl)
      this.controller.getData(this.state.nextUrl)
    this.setState({ loadBottom: true })
    this.controller.sendRemoteData(this.state.token)
    this.controller.sendRemoteNotification(this.state.token)
  }
  _renderRow(rowData) {
    return (
      <ActivityListCard data={rowData} onPressDelete={(item) => this.deleteIndex(item)} />
    );
  }
  toggleOpen = () => {
    if (this.state.open) {
      Router.hiddenComponent()
    }
    this.setState({ open: !this.state.open });
  };

  drawerContent = () => {
    return (
      <View style={styles.animatedBox}>
        <TouchableOpacity style={styles.backbuttoncontainer}
          onPress={() => this.setState({open:false})}
        >
          <View style={styles.buttonStyle}>
            <Image source={require('src/assets/newImage/left_arrow.png')}
              resizeMode="contain"
              style={styles.image} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.toggleOpen} style={styles.animatedBox} >
          <Text style={styles.subtext}>Hidden event</Text>
        </TouchableOpacity>
        {/* <TouchableOpacity onPress={()=> this.setState({open:false})} style={styles.animatedBox}>
        <Text style={styles.subtext}>Close Drawer</Text>
      </TouchableOpacity> */}
      </View>

    );
  };
  renderFooter() {
    return (
      //Footer View with Load More button
      <View style={styles.footer}>
        {!this.state.loadBottom ? <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => this.loadMoreData()}
          style={styles.loadMoreBtn}>
          <Text style={styles.btnText}>Load More</Text>
        </TouchableOpacity> :
          <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
        }
      </View>
    );
  }
  render() {
    const { refreshing } = this.state;
    return (
      <MenuDrawer
        open={this.state.open}
        drawerContent={this.drawerContent()}
        drawerPercentage={45}
        animationTime={250}
        overlay={true}
        opacity={0.4}
      >
        <ParallaxScrollView
          windowHeight={SCREEN_HEIGHT * 0.3}
          backgroundSource='http://i.imgur.com/UyjQBkJ.png'
          navBarTitle='Home'
          userImage='http://i.imgur.com/RQ1iLOs.jpg'
          leftIcon={{ name: 'menu', color: 'rgba(193, 193, 193, 1)', size: 25, }}
          leftIconOnPress={() => this.toggleOpen()}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={() => this.onRefresh()} />}

        >
          {this.state.listData.length ? <FlatList
            style={{ flex: 1 }}
            data={this.state.listData}
            renderItem={item => this._renderRow(item.item)}
            scrollEventThrottle={0}
            ItemSeparatorComponent={() => <View style={styles.separator} />}
            ListFooterComponent={this.renderFooter()}
          /> : <View />
          }
        </ParallaxScrollView>
      </MenuDrawer>
    );
  }
}

const mapStateToProps = ({ account }) => {
  const { hiddenData } = account;
  return { hiddenData };
};

export default connect(mapStateToProps, { hiddenDataChange })(HomeComponent);