
import {color,fontColor, resize } from 'src/Utilities/styleConfig';
import {StyleSheet} from 'react-native';

// 27AE60
const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      height: 400,
      width: 400,
      justifyContent: 'center',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
    maincontainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#27AE60',
    },
    logoicon: {
        marginTop: resize.verticalScale(200),
        width: resize.squire(100),
        height: resize.squire(100),
    },
    animatedBox: {
        flex: 1,
        backgroundColor: color.appColor,
        // padding: 10
      },
      body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F04812'
      },
    subtext:{
        marginTop: resize.verticalScale(10),
        marginLeft: resize.moderateScale(10),
        fontSize: resize.moderateScale(18),
        color: '#FFF',  
    },
    item: {
        padding: 10,height:80
      },
      separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
      },
      text: {
        fontSize: 15,
        color: 'black',
      },
      footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
      },
      loadMoreBtn: {
        padding: 10,
        backgroundColor: color.appColor,
        borderRadius: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: resize.verticalScale(30),
      },
      btnText: {
        color: 'white',
        fontSize: 15,
        textAlign: 'center',
      },
    image:{
        width: resize.width,
        height: resize.verticalScale(160),
    },
    modaltopcontainer: {
      flexDirection: 'row',
      width: '100%',
      paddingTop: resize.verticalScale(20),
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: resize.moderateScale(4, 0.5),
  },
  backbuttoncontainer: {
    // backgroundColor:null,
    // marginTop:esize.moderateScale(),
      height: resize.moderateScale(50),
      width: resize.width,
      alignItems: 'flex-start',
      justifyContent: 'center',
      paddingLeft: resize.moderateScale(12, 1),
  },
  buttonStyle: {
      height: resize.moderateScale(20, 1),
      width: resize.moderateScale(20, 1),
      opacity: 0.75,
      alignItems: 'center',
      justifyContent: 'center',
  },
  image: {
      marginLeft: resize.moderateScale(20),
      height: resize.squire(20),
      width: resize.squire(20),
  },
});

export default styles;