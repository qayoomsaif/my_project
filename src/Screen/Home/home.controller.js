import {
  Helper,
} from "src/Factory";
import { Services, error } from "src/Services";
import firebaseClient from './FirebaseClient'

export default class HomeComponent {
  constructor(object) {
    this.object = object;
  }
  async getData(url) {
    try {
      let response = await Services.dataServices.getDataFromApi(url);
      if(response.data){
        this.object.setData(response.data,response.next_page_url)
        return
      }
    } catch (e) {
      console.log(e)
    }
  }

  sendRemoteNotification(token) {
    // if (Platform.OS === "android") {
      let body = {
        to: token,
        data: {
          custom_notification: {
            title: "Hi qayoom",
            body: "This is a notification with only Api fetch request.",
            sound: "default",
            priority: "high",
            show_in_foreground: true,
            targetScreen: "homeComponent"
          }
        },
        priority: 10
      };
    // }
    firebaseClient.send(JSON.stringify(body), "notification");
  }

  sendRemoteData(token) {
    let body = {
      to: token,
      data: {
        title: "Hi qayoom",
        body: "This is a notification with only Api fetch request.",
        sound: "default"
      },
      priority: "normal"
    };

    firebaseClient.send(JSON.stringify(body), "data");
  }
  
}
