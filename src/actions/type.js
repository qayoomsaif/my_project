export const NAME_CHANGED = 'name_changed';
export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const CONFIRM_PASSWORD_CHANGED = 'confirm_password_changed';
export const HIDDEN_DATA_CHANGE = 'HIDDEN_DATA_CHANGE';
