import {   NAME_CHANGED, EMAIL_CHANGED, PASSWORD_CHANGED, CONFIRM_PASSWORD_CHANGED , HIDDEN_DATA_CHANGE } from './type';

export const nameChanged = (text) =>{
    return{
        type: NAME_CHANGED,
        payload: text
    };
};

export const emailChanged = (text) => {
    return{
        type: EMAIL_CHANGED,
        payload: text
    };
};
export const hiddenDataChange = (data) => {
    return{
        type: HIDDEN_DATA_CHANGE,
        payload: data
    };
};

export const passwordChanged = (text) => {
    return{
        type: PASSWORD_CHANGED,
        payload: text
    };
};

export const confirmPasswordChanged = (text) => {
    return{
        type: CONFIRM_PASSWORD_CHANGED,
        payload: text
    };
};