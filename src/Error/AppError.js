
import ExtendableBuiltin from './ExtendableBuiltin'
module.exports = class AppError extends ExtendableBuiltin(Error) {
  constructor(message, type) {
    super(message, type);
    this.mainType = 'AppError'
    this.type = type || AppError.name;
    this.message = message || 'Something went wrong. Please try again.'
  }
};