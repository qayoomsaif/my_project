
module.exports = class NetworkError extends require('./UserError') {
    constructor(message, type) {
        // Overriding both message and status code.
        super(message, type);
        // Saving custom property.
        this.mainType = 'NetworkError'
        this.type = type || 'NetworkError'
        this.message = message || 'Please check your internet connection and try again';
    }
};
