module.exports = class ResponseError extends require('./ApiError') {
    constructor(message, type) {
        super(message, type);
        super(message, type);
        this.type = type || 'ResponseError'
        this.message = message || 'Something went to wrong please try again later!';
    }
};
