module.exports = class ResponsNull extends require('./ApiError') {
    constructor(message, type) {
        super(message, type, ResponsNull.name);
        this.type = type || 'ResponsNull'
        this.message = message || 'Empty Data';
    }
};
