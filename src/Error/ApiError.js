
import ExtendableBuiltin from './ExtendableBuiltin'
module.exports = class ApiError extends ExtendableBuiltin(Error) {
  constructor(message, type) {
    super(message, type);
    this.mainType = 'ApiError'
    this.type = type || ApiError.name;
    this.message = message || 'Something went wrong. Please try again.'
  }
};
 