module.exports = class JsonError extends require('./ApiError') {
    constructor(message, type) {
        super(message, type);
        this.type = type || 'JsonError'
        this.message = message || 'Something went wrong. Please try again.';
    }
};
