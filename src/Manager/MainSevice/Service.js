import { ResponseError, ApiError } from 'src/Error'

export default class Service {
    constructor(serviceMain) {
        this.serviceMain = serviceMain
    }

    // Generic GET method
    async GET(api, param, header, contentType, DoNotUrlEncoded) {
        // console.log(api, param, header, contentType )
        var url, setCofig
        try {
            // receives full url, header and youcan Server request 
           var setCofig = await this.setCofig(api, header, contentType),
            url = api
            if (param) {
                    encodedData = await this.toFormUrlEncoded(param)
                    url = api += "?" + encodedData
            }
            let response = await this.serviceMain.GET(url, null, setCofig.header)
            return response
        } catch (e) {
            throw (e)
        }
    }


    async POST(api, param, header, contentType, DoNotUrlEncoded) {
        var body, setCofig
        try {
            setCofig = await this.setCofig(header, contentType)
            if (DoNotUrlEncoded) {
                body = JSON.stringify(param)
            } else {
                body = await this.toFormUrlEncoded(param)
            }
            let response = await this.serviceMain.POST(api, body, setCofig.header)
            return response
        } catch (e) {
            throw (e)
        }
    }

    // sets configs e.g header, contentType
    async  setCofig(header, contentType) {
        Content_Type = 'application/x-www-form-urlencoded'
        if (contentType == 1) {
            Content_Type = 'application/json'
        }
        var requestHeader = {
            'Accept': 'application/json', 'Content-Type': Content_Type,
        };
        if (header) {
            requestHeader = Object.assign(requestHeader, header)
        }
        return { header: requestHeader }
    }

    async toFormUrlEncoded(param) {
        let promise = new Promise(async (resolve, reject) => {
            const formBody = Object.keys(param)
                .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(param[key]))
                .join('&');
            return resolve(formBody);
        })
        return promise
    }
}
module.exports = { Service };