
import {
    NetworkError, ApiError, AppError, JsonError
} from 'src/Error'
export default class ServiceMain {
    constructor() {
    }

    async GET(url, body, header) {

        try {
            let response = await fetch(url, { method: 'GET', headers: header })
            let responseJson = await this.responseToJSon(response)
            if (responseJson.status === 404) { throw new AppError(responseJson.message) }
            if (response.status == 200) {
                if (responseJson.error) {
                    if (responseJson.error.message) { throw new ApiError(responseJson.error.message) }
                    if (responseJson.error.stack) {
                        if (responseJson.error.stack.msg) { throw new ApiError(responseJson.error.stack.msg) }
                        throw new ApiError(responseJson.error.stack)
                    }
                    throw new ApiError(responseJson.error)
                }
                if (responseJson.result) { return responseJson.result }
                return responseJson
            }
            if (responseJson.error) {
                if (responseJson.error.message) { throw new ApiError(responseJson.error.message) }
                if (responseJson.error.stack) {
                    if (responseJson.error.stack.msg) { throw new ApiError(responseJson.error.stack.msg) }
                    throw new ApiError(responseJson.error.stack)
                }
                throw new ApiError(responseJson.error)
            }
            return responseJson
        } catch (e) {
            if (e.message === 'Network request failed') {
                throw new NetworkError()
            }
            throw (e)
        }
    }

    async responseToJSon(response) {
        try {
            let responseJson = await response.json()
            return responseJson

        } catch (e) {
            throw new JsonError()
        }
    }

    async POST(url, body, header, ouerSevereRequest) {
        try {
            let response = await fetch(url, { method: 'POST', headers: header, body: body })
            let responseJson = await this.responseToJSon(response)
            if (responseJson.status == 404) { throw new ApiError(responseJson.message) }
            if (response.status == 200) {
                if (responseJson.error) {
                    if (responseJson.error.message) { throw new ApiError(responseJson.error.message) }
                    if (responseJson.error.stack) {
                        if (responseJson.error.stack.msg) { throw new ApiError(responseJson.error.stack.msg) }
                        throw new ApiError(responseJson.error.stack)
                    }
                    throw new ApiError(responseJson.error)
                }
                if (responseJson.result) { return responseJson.result }
                return responseJson
            }
            if (responseJson.error) {
                if (responseJson.error.message) { throw new ApiError(responseJson.error.message) }
                if (responseJson.error.stack) {
                    if (responseJson.error.stack.msg) { throw new ApiError(responseJson.error.stack.msg) }
                    throw new ApiError(responseJson.error.stack)
                }
                throw new ApiError(responseJson.error)
            }
            return responseJson
        } catch (e) {
            if (e.message === 'Network request failed') {
                throw new NetworkError()
            }
            throw (e)
        }
    }

    isEmpty(object) {
        var isEmpty = true;
        for (keys in object) {
            isEmpty = false;
            break; // exiting since we found that the object is not empty
        }
        return isEmpty;
    }
}
module.exports = { ServiceMain }; 