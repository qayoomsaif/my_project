import { Alert } from "react-native";

export default class Helper {
  constructor(router) {
    this.router = router;
  }
  alert(message, title, buttons) {
    Alert.alert(title || "", message, buttons);
  }
  async validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,10}))$/;
    if (re.test(email)) {
      return true;
    }
    return false;
  }
  loginRequired(screenName = "unknown", data = null) {
    this.router.loginComponent({ propsData: { screenName, data } });
    // let button = [
    //     { text: 'Later', onPress: () => { } },
    //     { text: 'Login', onPress: () => this.router.loginComponent({ propsData: { screenName, data } }) },
    // ]
    // this.alert('Please login to proceed', 'Login Required!', button)
  }
  async validateNum(num) {
    regex = /^[0-9\s]*$/;
    // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,10}))$/;
    if (regex.test(num)) {
      return true;
    }
    return false;
  }
  async validateStringWithSpace(str) {
    regex = /^[a-zA-Z ]*$/;
    // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,10}))$/;
    if (regex.test(str)) {
      return true;
    }
    return false;
  }
  async validateOnlyString(str) {
    regex = /^[a-zA-Z]*$/;
    // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,10}))$/;
    if (regex.test(str)) {
      return true;
    }
    return false;
  }
  async validateNuminArry(num) {
    var value = true;
    for (i = 0; i < num.length; i++) {
      let regex = /^[0-9\s]*$/;
      let res = regex.test(num[i]);
      if (!res) {
        value = false;
      }
    }
    return value;
  }
}
module.exports = { Helper };
